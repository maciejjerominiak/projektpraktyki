package moja.biblioteka.database.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "BOOKS")
public class Book implements BaseModel{

    public Book() {
    }

    @DatabaseField(generatedId = true)
    private int id;


    @DatabaseField(columnName = "TITLE", canBeNull = false)
    private String title;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    }

