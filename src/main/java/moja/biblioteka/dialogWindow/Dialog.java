package moja.biblioteka.dialogWindow;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;
import java.util.ResourceBundle;

public class Dialog {

    static ResourceBundle bundle = ResourceBundle.getBundle("bundles.messages");

    public static void dialogAbout(){
        Alert informationAlert = new Alert(Alert.AlertType.INFORMATION);
        informationAlert.setTitle(bundle.getString("about.title"));
        informationAlert.setHeaderText(bundle.getString("about.header"));
        informationAlert.setContentText(bundle.getString("about.content"));
        informationAlert.showAndWait();
    }

    public static Optional<ButtonType> Exit(){
        Alert exitDialog = new Alert(Alert.AlertType.CONFIRMATION);
        exitDialog.setTitle(bundle.getString("exit.title"));
        exitDialog.setHeaderText(bundle.getString("exit.header"));
        Optional<ButtonType> result = exitDialog.showAndWait();
        return result;
    }

}
